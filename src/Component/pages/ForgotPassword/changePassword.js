import React, {Component} from 'react';
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  AsyncStorage,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import styles from '../Customer/style';
import Spinner from 'react-native-loading-spinner-overlay';
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter,
} from 'react-native-popup-dialog';
import {connect} from 'react-redux';
import {changeUserPassword} from './redux/forgotPasswordAction';
import {Icon} from 'react-native-elements';
import {TextField} from 'react-native-material-textfield';

class changePassword extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      customer_id: '',
      oldPassword: '',
      newPassword: '',
      newConfirmPassword: '',
      oldPassword_error: '',
      newPassword_error: '',
      newConfirmPassword_error: '',
      spinner: false,
      visible1: false,
      popupMsg: '',
      status: 0,
    };
  }

  //--FETCH LOGIN ID FROM ASYNCSTORAGE--//
  componentDidMount = async () => {
    console.log('componentdidmount', this.props);

    try {
      const login_id = await AsyncStorage.getItem('login_id_2');

      if (login_id) {
        this.setState({customer_id: login_id});
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  //--SET OLD PASSWORD--//
  oldPasswordHandler = oldPassword => {
    this.setState({
      oldPassword: oldPassword,
      oldPassword_error: '',
    });
  };

  //--SET NEW PASSWORD--//
  newPasswordHandler = newPassword => {
    this.setState({
      newPassword: newPassword,
    });
    if (this.state.newPassword.length < 5) {
      this.setState({
        newPassword_error: 'Password must be atleast 6 character',
      });
    } else {
      this.setState({newPassword_error: ''});
    }
  };

  //--COMFIRM NEW PASSWORD--//
  newConfirmPasswordHandler = newConfirmPassword => {
    this.setState({
      newConfirmPassword: newConfirmPassword,
      newConfirmPassword_error: '',
    });
  };

  //--CLOSE POPUP BOX--//
  closePopupbox = () => {
    this.setState({
      visible1: false,
    });
    if(this.state.status==1){
    this.props.navigation.navigate('myAccount');
    }
  };

  //--VALIDATION AND CHANGE PASSWORD USING REDUX--//
  handleSubmit = () => {
    if (this.state.oldPassword == '') {
      this.setState({
        oldPassword_error: 'Enter Old Password',
      });
    } else if (this.state.newPassword == '') {
      this.setState({
        newPassword_error: 'Enter New Password',
      });
    } 
    else if (this.state.newConfirmPassword == '') {
      this.setState({
        newConfirmPassword_error: 'Enter New Confirm Password',
      });
    } 
    else if (this.state.oldPassword == this.state.newPassword) {
      this.setState({
        newPassword_error: 'Enter any other password except the old one',
      });
    } 
    else if (this.state.newPassword != this.state.newConfirmPassword) {
      this.setState({newConfirmPassword_error: 'New Password not matched'});
    } else if (this.state.newPassword.length < 6) {
      this.setState({
        newPassword_error: 'Password must be atleast 6 character',
      });
    } else {
      this.setState({spinner: true});

      let newPassword = new FormData();
      newPassword.append('id', this.state.customer_id);
      newPassword.append('old_pass', this.state.oldPassword);
      newPassword.append('new_pass', this.state.newPassword);
      newPassword.append('con_pass', this.state.newPassword);

      this.props.changeUserPassword(newPassword);
    }
  };

  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) {
      const addDetails = this.props.changePasswordUserDetails;
      if (this.props.error && this.props.error !== undefined) {
        if (this.state.visible1 === false) {
          this.setState({visible1: true, popupMsg: addDetails.msg});
        }
        this.setState({spinner: false});
      } else {
        if (addDetails.ack === 1) {
          this.setState({spinner: false,
          status: 1});
          if (this.state.visible1 === false) {
            this.setState({visible1: true, popupMsg: addDetails.msg});
          }
        } else {
          this.setState({
            spinner: false,
          });
          if (this.state.visible1 === false) {
            this.setState({visible1: true, popupMsg: addDetails.msg});
          }
        }
      }
    }
  }

  render() {
    const {goBack} = this.props.navigation;
    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="transparent" />
        <KeyboardAvoidingView enabled behavior="height">
          <View style={[styles.topbar]}>
            <View style={styles.justifyrow}>
              <TouchableOpacity onPress={() => goBack()}>
                <Icon name="md-arrow-back" type="ionicon" color="#fff" />
              </TouchableOpacity>
              <Text style={styles.backheading}>Security Settings</Text>
            </View>
          </View>
          <ScrollView>
            <View style={{position: 'relative', paddingBottom: 40}}>
              <View style={styles.graycontainer}>
                <Spinner
                  visible={this.state.spinner}
                  textContent={'Loading...'}
                  textStyle={styles.spinnerTextStyle}
                />
                <View style={styles.graycard}>
                  <TextField
                    label="Old Password"
                    textColor={'#8f8f92'}
                    baseColor={'#fff'}
                    labelFontSize={18}
                    activeLineWidth={1}
                    value={this.state.oldPassword}
                    onChangeText={oldPassword =>
                      this.oldPasswordHandler(oldPassword)
                    }
                  />
                  <Text style={styles.error}>
                    {this.state.oldPassword_error}
                  </Text>
                  <TextField
                    label="New Password"
                    textColor={'#8f8f92'}
                    baseColor={'#8f8f92'}
                    labelFontSize={18}
                    activeLineWidth={1}
                    value={this.state.newPassword}
                    onChangeText={newPassword =>
                      this.newPasswordHandler(newPassword)
                    }
                    secureTextEntry={false}
                  />
                  <Text style={styles.error}>
                    {this.state.newPassword_error}
                  </Text>
                  <TextField
                    label="Confirm Password"
                    textColor={'#8f8f92'}
                    baseColor={'#8f8f92'}
                    labelFontSize={18}
                    activeLineWidth={1}
                    value={this.state.newConfirmPassword}
                    onChangeText={newConfirmPassword =>
                      this.newConfirmPasswordHandler(newConfirmPassword)
                    }
                    secureTextEntry={false}
                  />
                  <Text style={styles.error}>
                    {this.state.newConfirmPassword_error}
                  </Text>
                  <View style={styles.container}>
                    <Dialog
                      visible={this.state.visible1}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: 'bottom',
                        })
                      }
                      onTouchOutside={() => {
                        this.closePopupbox();
                      }}
                      dialogStyle={{width: '80%'}}
                      footer={
                        <DialogFooter>
                          <DialogButton
                            textStyle={{
                              fontSize: 14,
                              color: '#333',
                              fontWeight: '700',
                            }}
                            text="OK"
                            onPress={() => {
                              this.closePopupbox();
                            }}
                          />
                        </DialogFooter>
                      }>
                      <DialogContent>
                        <Text style={styles.popupText}>
                          {this.state.popupMsg}
                        </Text>
                      </DialogContent>
                    </Dialog>
                  </View>
                </View>

                <TouchableOpacity
                  style={[styles.pinkbtn, {marginTop: 30}]}
                  onPress={() => this.handleSubmit()}>
                  <Text style={styles.btntext}>Submit</Text>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.forgotPassword,
});

export default connect(
  mapStateToProps,
  {changeUserPassword},
)(changePassword);
