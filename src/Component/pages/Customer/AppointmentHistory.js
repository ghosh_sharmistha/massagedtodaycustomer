import React, { Component } from "react";
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Image,
  AsyncStorage,
  Alert,
  TextInput
} from "react-native";
import { Avatar, Icon, CheckBox } from "react-native-elements";
import styles from "./style";
import { connect } from "react-redux";
import {
  fetchAppointmentHistory,
  fetchCustomerDetails,
  addReview
} from "./redux/customerAction";
import Spinner from "react-native-loading-spinner-overlay";
import moment from "moment";

import Dialog, {
  DialogContent,
  DialogTitle,
  DialogFooter,
  DialogButton
} from "react-native-popup-dialog";
import StarRating from "react-native-star-rating";

class AppointmentHistory extends Component {
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props);
    this.state = {
      spinner: true,
      customer_id: "",
      visible: false,
      starCount: 0,
      starSpaName: "",
      starSpaImage: "",
      starSpaId: "",
      starSpaComment: ""
    };
    todayDate = moment(new Date()).format("YYYY-MM-DD");
  }

  componentWillReceiveProps(nextProps) {
    //console.log('componentWillReceiveProps', nextProps);
    this.setState({ spinner: false });
  }

  //--FETCH APPOINTMENT HOSTORY THROUGH REDUX--//
  async componentDidMount() {
    this.props.navigation.addListener("willFocus", () => {
      console.log("focus");
      if (this.state.customer_id) {
        console.log("focused working");
        let userDetails = new FormData();
        userDetails.append("customer_id", this.state.customer_id);
        userDetails.append("todaysDate", todayDate);

        this.props.fetchAppointmentHistory(userDetails);
        console.log("userDetails2 fous", userDetails);

        let userDetails2 = new FormData();
        userDetails2.append("user_id", this.state.customer_id);
        this.props.fetchCustomerDetails(userDetails2);
        console.log("userDetails2 focus", userDetails2);
      }
    });
    try {
      const value = await AsyncStorage.getItem("login_id_2");
      console.log("loginId in myaccount page", value);
      if (value) {
        this.setState({ customer_id: value });
      }
    } catch (error) {
      console.log(error);
      // Error retrieving data
    }
    let userDetails = new FormData();
    userDetails.append("customer_id", this.state.customer_id);
    userDetails.append("todaysDate", todayDate);

    this.props.fetchAppointmentHistory(userDetails);
    console.log("userDetails asyn", userDetails);

    let userDetails2 = new FormData();
    userDetails2.append("user_id", this.state.customer_id);
    this.props.fetchCustomerDetails(userDetails2);
    console.log("userDetails2 asyn", userDetails2);
  }

  componentDidUpdate(prevProps) {
    console.log(this.props);

    if (prevProps.reviewDetails !== this.props.reviewDetails) {
      this.setState({ spinner: false });
      console.log("if2");

      const addDetails = this.props.reviewDetails;
      if (this.props.error && this.props.error !== undefined) {
        console.log("if2");

        Alert.alert("Something went wrong.");
      } else {
        console.log("else1");

        if (addDetails.Ack === 1) {
          Alert.alert(addDetails.msg);
          this.props.navigation.navigate("myAccount");
        } else {
          Alert.alert("Refund Process. Something went wrong.");
        }
      }
    }
  }

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating
    });
  }

  addStarRating() {
    
    if(this.state.starCount == 0 || this.state.starSpaComment == ""){
      console.log("rate it");
    }
    else{
      this.setState({
        visible: false,
        starCount: 0
      });
      let reviewdata = new FormData();
      reviewdata.append("spa_id", this.state.starSpaId);
      reviewdata.append("customer_id", this.state.customer_id);
      reviewdata.append("rating", this.state.starCount);
      reviewdata.append("comment", this.state.starSpaComment);
      this.props.addReview(reviewdata);
      console.log("this.props", this.props);
      this.setState({ spinner: true });
    }
  }

  render() {
    //console.log('props in render in appointment history page', this.props);
    // console.log(
    //   'props in render in appointment history page',
    //   this.props.appointmentHistory,
    // );

    //const {goBack} = this.props.navigation;
    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="#1a1a1d" />
        <View style={[styles.topbar]}>
          <View style={styles.justifyrow}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("myAccount")}
            >
              <Icon name="md-arrow-back" type="ionicon" color="#fff" />
            </TouchableOpacity>
            <Text style={styles.backheading}>Appointment History</Text>
          </View>
        </View>

        <Dialog
          dialogStyle={{ width: "60%", marginTop: -200, justifyContent: 'center', alignItems: 'center' }}
          visible={this.state.visible}
          dialogTitle={<DialogTitle title={this.state.starSpaName} />}
          onTouchOutside={() => {
            this.setState({ visible: false });
          }}
          footer={
            <DialogFooter>
              <DialogButton
                text="Cancel"
                onPress={() => {
                  this.setState({
                    visible: false
                  });
                }}
              />
              <DialogButton
                text="Ok"
                //onPress={() => {}}
                onPress={() => this.addStarRating()}
              />
            </DialogFooter>
          }
        >
          <DialogContent>
            <View>
              <View
                style={[
                  styles.media,
                  {
                    marginBottom: 0,
                    width: "90%",
                    justifyContent: "center",
                    alignItems: "center"
                  }
                ]}
              >
                {/* <Avatar
                rounded
                source={{
                uri: `${this.state.starSpaImage}`,
                }}
                size={60}
                containerStyle={{
                borderWidth: 3,
                borderColor: '#676782',
                padding: 3,
                backgroundColor: '#fff',
                }}
                /> */}
                {/* <Text style={{fontWeight: 'bold', color: '#e25cff', fontSize: 20, marginTop: 8}}>{this.state.starSpaName}</Text> */}
              </View>
              <StarRating
                disabled={false}
                emptyStar={"ios-star-outline"}
                fullStar={"ios-star"}
                halfStar={"ios-star-half"}
                iconSet={"Ionicons"}
                maxStars={5}
                rating={this.state.starCount}
                selectedStar={rating => this.onStarRatingPress(rating)}
                fullStarColor={"#ffcc00"}
              />
              <View style={styles.textAreaContainer}>
                <TextInput
                  style={styles.textArea}
                  underlineColorAndroid="transparent"
                  placeholder="Add a comment"
                  placeholderTextColor="grey"
                  numberOfLines={5}
                  multiline={true}
                  onChangeText={comment => {
                    this.setState({
                      starSpaComment: comment
                    });
                  }}
                />
              </View>
            </View>
          </DialogContent>
        </Dialog>
        <Spinner
          visible={this.state.spinner}
          textContent={"Loading..."}
          textStyle={styles.spinnerTextStyle}
        />
        <ScrollView>
          {/* {this.props.error && this.props.error !== undefined ? (
            Alert.alert('Something went wrong')
          ) : ( */}
          <View style={styles.graycontainer}>
            {this.props.appointmentHistory.length ? (
              this.props.appointmentHistory.map((item, key) => (
                <View key={key} style={styles.graycard}>
                  <View
                    style={[styles.media, { marginBottom: 0, width: "90%" }]}
                  >
                    <Avatar
                      rounded
                      source={{
                        uri: `${this.props.customerDetails.profile_image}`
                      }}
                      size={60}
                      containerStyle={{
                        borderWidth: 3,
                        borderColor: "#676782",
                        padding: 3,
                        backgroundColor: "#fff"
                      }}
                    />
                    <View style={styles.mediabody}>
                      <Text style={styles.h3}>{item.spa_name}</Text>

                      <View
                        style={[
                          styles.justifyrow,
                          { marginTop: 5, flexWrap: "wrap" }
                        ]}
                      >
                        <View style={styles.justifyrow}>
                          <Image
                            source={require("../../assets/images/calender.png")}
                            resizeMode="contain"
                            style={{ width: 14, marginRight: 5 }}
                          />
                          <Text style={styles.mutetext2}>
                            {item.booking_date}
                          </Text>
                        </View>
                        <View style={styles.justifyrow}>
                          <Image
                            source={require("../../assets/images/clock.png")}
                            resizeMode="contain"
                            style={{ width: 14, marginRight: 5 }}
                          />
                          <Text style={styles.mutetext2}>
                            {item.booking_start_time}
                          </Text>
                        </View>
                        <View style={styles.justifyrow}>
                          <Image
                            source={require("../../assets/images/setting.png")}
                            resizeMode="contain"
                            style={{ width: 14, marginRight: 5 }}
                          />
                          <Text style={styles.mutetext2}>{item.services}</Text>
                        </View>
                      </View>
                      {this.props.customerDetails &&
                      Object.keys(this.props.customerDetails).length ? (
                        <View style={styles.justifyrow}>
                          {this.props.customerDetails.review.includes(
                            item.spa_id
                          ) ? (
                            <View style={{ flexDirection: "row" }}>
                              <Image
                                source={require("../../assets/images/tick.png")}
                                resizeMode="contain"
                                style={{
                                  width: 14,
                                  height: 12,
                                  marginRight: 5,
                                  paddingBottom: 20
                                }}
                              />
                              <Text style={styles.mutetext2}>
                                {" "}
                                Review Added
                              </Text>
                            </View>
                          ) : (
                            <TouchableOpacity
                              style={styles.pinkbtn1}
                              onPress={() => {
                                this.setState({
                                  visible: true,
                                  starSpaName: item.spa_name,
                                  starSpaImage: this.props.customerDetails
                                    .profile_image,
                                  starSpaId: item.spa_id
                                });
                              }}
                            >
                              <Text style={styles.btntext}>Add Review</Text>
                            </TouchableOpacity>
                          )}
                          {/* <TouchableOpacity
                             onPress={() => {
                                this.setState({ 
                                    visible: true,
                                    starSpaName: item.spa_name,
                                    starSpaImage: this.props.customerDetails.profile_image,
                                    starSpaId: item.spa_id,
                                 });
                                }}>
                                <Text style={styles.mutetext2}>Add Review</Text>
                            </TouchableOpacity>                            */}
                        </View>
                      ) : null}
                    </View>
                  </View>
                </View>
              ))
            ) : (
              <Text style={styles.backheading}>
                No Appointment History Found
              </Text>
            )}
          </View>
          {/* )} */}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.customer
});
export default connect(mapStateToProps, {
  fetchAppointmentHistory,
  fetchCustomerDetails,
  addReview
})(AppointmentHistory);
