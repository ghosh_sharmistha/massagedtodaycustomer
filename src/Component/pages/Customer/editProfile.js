import React, {Component} from 'react';
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Image,
  KeyboardAvoidingView,
  Button,
  TextInput,
  AsyncStorage,
} from 'react-native';
import { TextInputMask } from 'react-native-masked-text'
import {Avatar, Icon} from 'react-native-elements';
import {TextField} from 'react-native-material-textfield';
import styles from './style';
import ImagePicker from 'react-native-image-picker';
import Spinner from 'react-native-loading-spinner-overlay';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import {fetchCustomerDetails, customerUpdate} from './redux/customerAction';
import {connect} from 'react-redux';
//import AsyncStorage from '@react-native-community/async-storage';
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter,
} from 'react-native-popup-dialog';
import Axios from 'axios';

class editProfile extends Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.state = {
      buttonDisabled: false,
      first_name: '',
      firstName_error: '',
      last_name: '',
      lastName_error: '',
      email: '',
      email_error: '',
      phone: '',
      phone_error: '',
      spinner: false,
      visible1: false,
      popupMsg: '',
      status: 0,
      city: '',
    };

    this.locationName();
    this.fetchCustomerData();
  }

  async fetchCustomerData() {
    const value = await AsyncStorage.getItem('login_id_2');
    let userDetails = new FormData();
    userDetails.append('user_id', value);
    if (this.props.fetchCustomerDetails(userDetails)) {
      this.setState({
        first_name: this.props.customerDetails.first_name,
        last_name: this.props.customerDetails.last_name,
        email: this.props.customerDetails.email,
        phone: this.props.customerDetails.phone,
        gender: this.props.customerDetails.gender,
        profile_image: this.props.customerDetails.profile_image,
      });
    }
  }

  componentDidUpdate(prevProps) {
    console.log('componentDidUpdate', this.props);
    if (prevProps.editProfileDetails !== this.props.editProfileDetails) {
      const addDetails = this.props.editProfileDetails;
      if (this.props.error && this.props.error !== undefined) {
        if (this.state.visible1 === false) {
          this.setState({visible1: true, popupMsg: addDetails.msg});
        }
        this.setState({spinner: false});
      } else {
        if (addDetails.Ack === 1) {
          this.setState({
            status: 1,
          });
          if (this.state.visible1 === false) {
            this.setState({
              visible1: true,
              popupMsg: addDetails.msg,
            });
          }
          //setTimeout(()=>{this.props.navigation.navigate('myAccount')}, 1000);
        } else {
          this.setState({
            spinner: false,
          });
          if (this.state.visible1 === false) {
            this.setState({
              visible1: true,
              popupMsg: 'Something went wrong. Please try again later.',
            });
          }
        }
      }
    }
  }

  locationName = () => {
    //current location fetch

    Axios.get('http://ip-api.com/json')
      .then(location => {
        console.log('location', location);
        this.setState({city: location.data.city});
      })
      .catch(err => {});
  };

  closePopupbox = () => {
    this.setState({
      visible1: false,
    });
    console.log('status', this.state.status);

    if (this.state.status == 1) {
      this.props.navigation.navigate('myAccount');
    }
  };

  handlechoosePhoto = () => {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        let source = {uri: response.uri};

        this.setState({
          ImageSource: source,
          data: response.data,
        });
      }
    });
  };

  onSelect = (index, value) => {
    console.log("value",value);
    console.log("index",index);
    
    this.setState({
      text: `Selected index: ${index} , value: ${value}`,
    });
    this.setState({gender: value});
  };

  validate = (text, type) => {
    if (type === 'first_name') {
      this.setState({first_name: text});
      this.forceUpdate();
      var regex = /^[a-zA-Z ]{2,30}$/;
      if (regex.test(this.state.first_name)) {
        this.setState({firstName_error: ''});
        this.setState({buttonDisabled: false});
        return true;
      } else {
        this.setState({firstName_error: 'special characters are not allowed.'});
        this.setState({buttonDisabled: true});
      }
      console.log(this.state);
    } else if (type == 'last_name') {
      this.setState({last_name: text});
      var regex = /^[a-zA-Z ]{2,30}$/;
      if (regex.test(this.state.last_name)) {
        this.setState({lastName_error: ''});
        this.setState({buttonDisabled: false});
        return true;
      } else {
        this.setState({lastName_error: 'special characters are not allowed.'});
        this.setState({buttonDisabled: true});
      }
    } else if (type == 'email') {
      this.setState({email: text});
      var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if (regex.test(this.state.email)) {
        this.setState({email_error: ''});
        this.setState({buttonDisabled: false});
        return true;
      } else {
        this.setState({
          email_error: 'You should provide proper email address.',
        });
        this.setState({buttonDisabled: true});
      }
    } else if (type == 'phone') {
      this.setState({phone: text, phone_error: ''});
      console.log("ph no length",text.length);
      if( text.length < 14 ){
        this.setState({phone_error: 'Please enter proper ph number'});
      }
      // var regex = /^(0|[1-9][0-9]{9})$/i;
      // if (regex.test(this.state.phone)) {
      //   this.setState({phone_error: ''});
      //   this.setState({buttonDisabled: false});
      //   return true;
      // } else {
      //   this.setState({phone_error: 'You should provide proper phone no.'});
      //   this.setState({buttonDisabled: true});
      // }
    }
  };

  submit = () => {
    if (this.state.email === '') {
      this.setState({
        email_error: 'Enter email id',
      });
    } else if (this.state.first_name === '') {
      this.setState({
        firstName_error: 'Enter First Name',
      });
    } else if (this.state.last_name === '') {
      this.setState({
        lastName_error: 'Enter Last Name',
      });
    } else if (this.state.phone === '') {
      this.setState({
        phone_error: 'Enter Phone Number',
      });
    } else {
      console.log("this.state.gender",this.state.gender);
      
      // this.setState({spinner: true});
      let userDetails = new FormData();
      userDetails.append('id', this.props.customerDetails.id);
      userDetails.append('email', this.state.email);
      userDetails.append('first_name', this.state.first_name);
      userDetails.append('last_name', this.state.last_name);
      userDetails.append('phone', this.state.phone);
      userDetails.append(
        'profile_image',
        this.state.data ? this.state.data : '',
      );
      userDetails.append(
        'gender',
        this.state.gender
      );
      console.log(userDetails);
      this.props.customerUpdate(userDetails);
    }
  };
  render() {
    // console.log(this.props.customerDetails)
    // console.log(this.state)
    const {goBack} = this.props.navigation;
    const {ImageSource} = this.state;
    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="#1a1a1d" />
        <View style={[styles.topbar]}>
          <View style={styles.justifyrow}>
            <TouchableOpacity onPress={() => goBack()}>
              <Icon name="md-arrow-back" type="ionicon" color="#fff" />
            </TouchableOpacity>
            <Text style={styles.backheading}>Edit Profile</Text>
          </View>
        </View>
        <ScrollView>
          <View style={{position: 'relative', paddingBottom: 40}}>
            <ImageBackground
              source={require('../../assets/images/spaEditProfile.jpeg')}
              style={styles.etprofile}></ImageBackground>
            <View style={styles.graycontainer}>
              <Spinner
                visible={this.state.spinner}
                textContent={'Loading...'}
                textStyle={styles.spinnerTextStyle}
              />
              <View
                style={[
                  styles.graycard,
                  {position: 'relative', top: -70, marginBottom: -50},
                ]}>
                <View style={[styles.media, {marginBottom: 0}]}>
                  {ImageSource ? (
                    <Avatar
                      rounded
                      onPress={this.handlechoosePhoto}
                      //source= {{ uri:photo }}
                      source={this.state.ImageSource}
                      size={70}
                      containerStyle={{
                        borderWidth: 3,
                        borderColor: '#676782',
                        padding: 4,
                        backgroundColor: '#fff',
                      }}
                    />
                  ) : this.props.customerDetails.profile_image !== null ? (
                    <Avatar
                      rounded
                      onPress={this.handlechoosePhoto}
                      source={{uri: this.props.customerDetails.profile_image}}
                      size={70}
                      containerStyle={{
                        borderWidth: 3,
                        borderColor: '#676782',
                        padding: 4,
                        backgroundColor: '#fff',
                      }}
                    />
                  ) : (
                    <Avatar
                      rounded
                      onPress={this.handlechoosePhoto}
                      source={require('../../assets/images/avt3.png')}
                      size={70}
                      containerStyle={{
                        borderWidth: 3,
                        borderColor: '#676782',
                        padding: 4,
                        backgroundColor: '#fff',
                      }}
                    />
                  )}
                  <View style={[styles.mediabody, {width: '50%'}]}>
                    <Text style={[styles.h3, {marginTop: 10}]}>
                      {this.props.customerDetails.name}
                    </Text>
                    <View style={styles.justifyrow}>
                      <View style={styles.justifyrow}>
                        <Image
                          source={require('../../assets/images/marker.png')}
                          resizeMode="contain"
                          style={{width: 16, marginRight: 5}}
                        />
                        <Text style={styles.mutetext}>{this.state.city}</Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.graycard}>
                <TextField
                  label="First Name"
                  defaultValue={this.props.customerDetails.first_name}
                  textColor={'#8f8f92'}
                  baseColor={'#fff'}
                  labelFontSize={18}
                  activeLineWidth={1}
                  onChangeText={text => this.validate(text, 'first_name')}
                  name="first_name"
                />
                <Text style={styles.error}>{this.state.firstName_error}</Text>

                <TextField
                  label="Last Name"
                  defaultValue={this.props.customerDetails.last_name}
                  textColor={'#8f8f92'}
                  baseColor={'#fff'}
                  labelFontSize={18}
                  activeLineWidth={1}
                  onChangeText={text => this.validate(text, 'last_name')}
                  name="last_name"
                />
                <Text style={styles.error}>{this.state.lastName_error}</Text>

                <TextField
                  label="Email"
                  defaultValue={this.props.customerDetails.email}
                  textColor={'#8f8f92'}
                  baseColor={'#fff'}
                  labelFontSize={18}
                  activeLineWidth={1}
                  editable={false}
                  onChangeText={text => this.validate(text, 'email')}
                  name="email"
                />
                <Text style={styles.error}>{this.state.email_error}</Text>
                
                <View style={{borderBottomColor: '#838389' , borderBottomWidth: 1, paddingBottom :8}}>
                <Text style={{paddingBottom: 8, color: '#838389', fontSize: 18 }}>Phone</Text>
                <TextInputMask
                  type={'cel-phone'}
                  options={{
                    maskType: 'BRL',
                    withDDD: true,
                    dddMask: '(999) 999-9999 '
                  }}
                  maxLength={14}
                  style={{color: '#838389'}}
                  value={this.state.phone}
                  onChangeText={value => this.validate(value, 'phone')}
                />
                </View>
                {/* <TextField
                  label="Phone"
                  defaultValue={this.props.customerDetails.phone}
                  textColor={'#8f8f92'}
                  keyboardType={'numeric'}
                  baseColor={'#fff'}
                  labelFontSize={18}
                  activeLineWidth={1}
                  onChangeText={text => this.validate(text, 'phone')}
                  name="phone"
                /> */}
                <Text style={styles.error}>{this.state.phone_error}</Text>
                <Dialog
                  visible={this.state.visible1}
                  dialogAnimation={
                    new SlideAnimation({
                      slideFrom: 'bottom',
                    })
                  }
                  onTouchOutside={() => {
                    this.closePopupbox();
                  }}
                  dialogStyle={{width: '80%'}}
                  footer={
                    <DialogFooter>
                      <DialogButton
                        textStyle={{
                          fontSize: 14,
                          color: '#333',
                          fontWeight: '700',
                        }}
                        text="OK"
                        onPress={() => {
                          this.closePopupbox();
                        }}
                      />
                    </DialogFooter>
                  }>
                  <DialogContent>
                    <Text style={styles.popupText}>{this.state.popupMsg}</Text>
                  </DialogContent>
                </Dialog>
                <Text style={styles.radioText}>Gender</Text>
                <RadioGroup
                  onSelect={(index, value) => this.onSelect(index, value)}
                  selectedIndex={this.props.customerDetails.gender}>
                  <RadioButton value={0}>
                    <Text style={styles.radioTextItem}>Male</Text>
                  </RadioButton>

                  <RadioButton value={1}>
                    <Text style={styles.radioTextItem}>Female</Text>
                  </RadioButton>
                </RadioGroup>

                <Button
                  style={[
                    {paddingBottom: 30},
                    {marginBottom: 30},
                    {marginTop: 30},
                  ]}
                  onPress={this.submit}
                  disabled={this.state.buttonDisabled}
                  title="Submit"
                />
              </View>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.customer,
});
export default connect(mapStateToProps, {fetchCustomerDetails, customerUpdate})(
  editProfile,
);
