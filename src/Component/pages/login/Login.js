import React, { Component } from "react";
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
  ImageBackground,
  AsyncStorage,
  Image,
  TextInput
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import styles from "./style";
import Spinner from "react-native-loading-spinner-overlay";
import { Button, CheckBox } from "react-native-elements";

import { connect } from "react-redux";
import { login, fbLogin , googleLogin} from "./redux/authAction";
//import AsyncStorage from '@react-native-community/async-storage';
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter
} from "react-native-popup-dialog";
import { FBLogin, FBLoginManager} from "react-native-facebook-login";
import { throwStatement } from "@babel/types";
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';

GoogleSignin.configure({
  //scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
  webClientId: '465387625959-tfahmkouv17nak16ffoqnq8trkjskgo2.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
  offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
  hostedDomain: '', // specifies a hosted domain restriction
  loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
  forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login.
  accountName: '', // [Android] specifies an account name on the device that should be used
  //iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
});

class LoginScreen extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      email_error: "",
      password_error: "",
      login_userid: "",
      spinner: false,
      visible1: false,
      visible2: false,
      popupMsg: "",
      fb_signup_id: "",
      first_name: "",
      last_name: "",
      name: "",
      phone: "",
      checkbox: false,
      mark: 1,
      facebookButton: 1,
      //visible3: false,
      isSigninInProgress: false
    };
    //this.fbLogin = this.fbLogin.bind(this);
    // this.remember();
  }

  fbLogin = data => {
    console.log("facebook login data", data);

    if (data) {
      if (data.profile.email) {
        console.log("facebook login data", data.profile.phone);
        let facebookDetails = new FormData();
        facebookDetails.append(
          "first_name",
          data.profile.first_name ? data.profile.first_name : ""
        );
        facebookDetails.append(
          "last_name",
          data.profile.last_name ? data.profile.last_name : ""
        );
        facebookDetails.append(
          "name",
          data.profile.name ? data.profile.name : ""
        );
        facebookDetails.append(
          "email",
          data.profile.email ? data.profile.email : ""
        );
        facebookDetails.append(
          "phone",
          data.profile.phone ? data.profile.phone : ""
        );
        facebookDetails.append(
          "fb_signup_id",
          data.profile.id ? data.profile.id : ""
        );
        console.log("facebookDetails", facebookDetails);
        this.setState({ spinner: true });
        this.props.fbLogin(facebookDetails);
      } else {
        console.log("no");
        this.setState({
          first_name: data.profile.first_name ? data.profile.first_name : "",
          last_name: data.profile.last_name ? data.profile.last_name : "",
          name: data.profile.name ? data.profile.name : "",
          phone: data.profile.phone ? data.profile.phone : "",
          fb_signup_id: data.profile.id ? data.profile.id : ""
        });
        this.setState({ visible2: true });
      }
    }
  };

  // async emailHandler(email) {
  //   console.log("mark value", this.state.mark);
  //   this.setState({ facebookButton: 2 });

  //   if (this.state.mark == 1) {
  //     const emailAsyncStorage = await AsyncStorage.getItem("loginEmail");
  //     const passwordAsyncStorage = await AsyncStorage.getItem("loginPassword");

  //     console.log("loginId------", emailAsyncStorage);
  //     console.log("loginId------", passwordAsyncStorage);

  //     if (emailAsyncStorage == null) {
  //       console.log("null");
  //       this.setState({ email: email });
  //     } else {
  //       console.log("checkbox");
  //       this.setState({
  //         visible3: true,
  //         popUPemail: emailAsyncStorage,
  //         popUPpassword: passwordAsyncStorage
  //       });
  //     }

  //     this.setState({ mark: 2 });
  //   } else {
  //     this.setState({ email: email, password: "" });
  //     let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  //     if (!reg.test(email)) {
  //       this.setState({
  //         email_error: "Enter valid email id"
  //       });
  //     } else {
  //       this.setState({
  //         email_error: ""
  //       });
  //     }
  //   }
  // }

  // emailChoose = () => {
  //   const chose_email = this.state.popUPemail;
  //   const chose_pass = this.state.popUPpassword;
  //   this.setState({
  //     checkbox: true,
  //     email: chose_email,
  //     password: chose_pass,
  //     email_error: "",
  //     visible3: false
  //   });
  // };

  // close = () => {
  //   this.setState({ visible3: false });
  //   //this.checkboxHandler();
  // };

  // passwordHandler = password => {
  //   this.setState({
  //     password: password,
  //     password_error: "",
  //     facebookButton: 2
  //   });
  // };

  // checkboxHandler = () => {
  //   if (this.state.email == "") {
  //     this.setState({ email_error: "Enter email id", facebookButton: 2});
  //   }
  //   //setcheckbox(checkbox);
  //   else if (this.state.checkbox == true) {
  //     //setcheckbox(false);
  //     this.setState({ checkbox: false });
  //     //AsyncStorage.removeItem("loginEmail");
  //    // AsyncStorage.removeItem("loginPassword");
  //   } else {
  //     // setcheckbox(true);
  //     this.setState({ checkbox: true });
  //    // AsyncStorage.setItem("loginEmail", this.state.email);
  //    // AsyncStorage.setItem("loginPassword", this.state.password);
  //   }
  // };

  // handleSubmit = () => {
  //   this.setState({facebookButton: 2})
  //   if (this.state.email === "") {
  //     this.setState({
  //       email_error: "Enter email id"
  //     });
  //   }
  //   if (this.state.password === "") {
  //     this.setState({
  //       password_error: "Enter password"
  //     });
  //   } else {

  //     if (this.state.checkbox == false) {
  //       AsyncStorage.removeItem("loginEmail");
  //       AsyncStorage.removeItem("loginPassword");
  //     } else {
  //       AsyncStorage.setItem("loginEmail", this.state.email);
  //       AsyncStorage.setItem("loginPassword", this.state.password);
  //     }

  //     this.setState({ spinner: true });
  //     let userDetails = new FormData();
  //     userDetails.append("email", this.state.email);
  //     userDetails.append("password", this.state.password);
  //     this.props.login(userDetails);
  //   }
  // };

  // componentDidUpdate(prevProps) {
  //   if (prevProps !== this.props) {
  //     if (this.props.error && this.props.error !== undefined) {
  //       if (this.state.visible1 === false) {
  //         this.setState({
  //           visible1: true,
  //           popupMsg: "Something went wrong,please try again"
  //         });
  //       }
  //       this.setState({ spinner: false });
  //     } else {
  //       if (this.props.userDetails && this.props.userDetails !== undefined) {
  //         if (this.props.userDetails.ack === 1) {
  //           this.setState({ spinner: false });
  //           if (this.props.userDetails.userdetail.user_type == "C") {
  //             this.storedata();
  //             //this.props.navigation.navigate('index');
  //           } else {
  //             this.setState({
  //               visible1: true,
  //               popupMsg:
  //                 "Cannot login successfully as you are not a register customer"
  //             });
  //           }
  //         } else {
  //           this.setState({ spinner: false });
  //           if (this.state.visible1 === false) {
  //             this.setState({
  //               visible1: true,
  //               popupMsg: this.props.userDetails.msg
  //             });
  //           }
  //         }
  //       } else if (this.props.fbLogin && this.props.fbLogin !== undefined) {
  //         console.log("this.props.fbLogin---------", this.props.fbLogin);
  //       } else {
  //         this.setState({ spinner: false });

  //         this.setState({
  //           visible1: true,
  //           popupMsg: "Not an valid customer"
  //         });
  //       }
  //     }
  //   }
  // }

  // async storedata() {
  //   try {
  //     console.log("testid1", this.state.login_userid);

  //     const value = await AsyncStorage.getItem("login_id");
  //     console.log("loginId------", this.props.userDetails.userId);
  //     await AsyncStorage.setItem(
  //       "login_id_2",
  //       String(this.props.userDetails.userId)
  //     );
  //     this.props.navigation.navigate("index");
  //   } catch (error) {
  //     console.log(error);
  //     // Error saving data
  //   }
  // }

  fbLogin2 = () => {
    if (this.state.email == "") {
      this.setState({ email_error: "Enter email" });
    } else if (this.state.email_error == "") {
      this.setState({ visible2: false });
      console.log("fologin", this.state.email);
      let facebookDetails = new FormData();
      facebookDetails.append("first_name", this.state.first_name);
      facebookDetails.append("last_name", this.state.last_name);
      facebookDetails.append("name", this.state.name);
      facebookDetails.append("email", this.state.email);
      facebookDetails.append("phone", this.state.phone);
      facebookDetails.append("fb_signup_id", this.state.fb_signup_id);
      console.log("facebookDetails ai email", facebookDetails);
      this.setState({ spinner: true });
      this.props.fbLogin(facebookDetails);
    }
  };

  // activeFbBottun = () => {
  //   console.log("fbactive",this.state.email);
  //   console.log("fbactive",this.state.password);

  //   if(this.state.email == ''){
  //     this.setState({facebookButton : 1})
  //   }
  // }

  

  // async remember() {
  //   try {
  //     const emailAsyncStorage = await AsyncStorage.getItem("loginEmail");
  //     const passwordAsyncStorage = await AsyncStorage.getItem("loginPassword");

  //     console.log("loginId------", emailAsyncStorage);
  //     if (emailAsyncStorage == null) {
  //       console.log("null");
  //       this.setState({ checkbox: false });
  //     } else {
  //       console.log("checkbox");
  //       this.setState({ checkbox: true });
  //     }

  //     this.setState({
  //       email: emailAsyncStorage,
  //       password: passwordAsyncStorage
  //     });
  //   } catch (error) {
  //     console.log(error);
  //     // Error saving data
  //   }

  // }

  // componentDidMount() {
  //   console.log(this.props);
  //   this.props.navigation.addListener("willFocus", () => {
  //     console.log("focus");
  //     //this.remember();
  //     console.log("emailAsyncStorage", this.state.email);
  //     console.log("emailAsyncStorage", this.state.password);
  //   });
  // }


  async emailHandler(email) {
    console.log("mark value", this.state.mark);
    this.setState({ facebookButton: 2 });

    if (this.state.mark == 1) {
      const emailAsyncStorage = await AsyncStorage.getItem("loginEmail");
      const passwordAsyncStorage = await AsyncStorage.getItem("loginPassword");

      if (emailAsyncStorage == null) {
        console.log("null");
        this.setState({ email: email });
      } else{
        console.log("checkbox");
        this.setState({
          checkbox: true,
          email: emailAsyncStorage,
          password: passwordAsyncStorage

        });
      }

      this.setState({ mark: 2 });
    } else {
      this.setState({ email: email, password: "" });
      let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if (!reg.test(email)) {
        this.setState({
          email_error: "Enter valid email id"
        });
      } else {
        this.setState({
          email_error: ""
        });
      }
    }
  }

  passwordHandler = password => {
    this.setState({
      password: password,
      password_error: ""
    });
  };

  checkboxHandler = () => {
    if (this.state.email == "") {
      this.setState({ email_error: "Enter email id", facebookButton: 2});
    }
    else if (this.state.checkbox == true) {
      this.setState({ checkbox: false });
    } else {
      this.setState({ checkbox: true });
    }
  };

  handleSubmit = () => {
    if (this.state.email === "") {
      this.setState({
        email_error: "Enter email id"
      });
    }
    if (this.state.password === "") {
      this.setState({
        password_error: "Enter password"
      });
    } else {

      if (this.state.checkbox == false) {
        AsyncStorage.removeItem("loginEmail");
        AsyncStorage.removeItem("loginPassword");
      } else {
        AsyncStorage.setItem("loginEmail", this.state.email);
        AsyncStorage.setItem("loginPassword", this.state.password);
      }

      this.setState({ spinner: true });
      let userDetails = new FormData();
      userDetails.append("email", this.state.email);
      userDetails.append("password", this.state.password);
      this.props.login(userDetails);
    }
  };

  componentDidUpdate(prevProps) {
    if (prevProps.userDetails !== this.props.userDetails) {
      if (this.props.error && this.props.error !== undefined) {
        if (this.state.visible1 === false) {
          this.setState({
            visible1: true,
            popupMsg: "Something went wrong,please try again"
          });
        }
        this.setState({ spinner: false });
      } else {
        if (this.props.userDetails && this.props.userDetails !== undefined && Object.keys(this.props.userDetails).length ) {
          console.log("didurdate",this.props);
          
          this.setState({ spinner: false });
          if (this.props.userDetails.ack === 1) {
            
            this.setState({ spinner: false });
            if (this.props.userDetails.userdetail.user_type == "C") {
              this.storedata();
              //this.props.navigation.navigate('index');
            } else {
              this.setState({
                visible1: true,
                popupMsg:
                  "Cannot login successfully as you are not a register customer"
              });
            }
          } else {
            this.setState({ spinner: false });
            if (this.state.visible1 === false) {
              this.setState({
                visible1: true,
                popupMsg: this.props.userDetails.msg
              });
            }
          }
        } else if (this.props.fbLogin && this.props.fbLogin !== undefined) {
          console.log("this.props.fbLogin---------", this.props.fbLogin);
        } else {
          this.setState({ spinner: false });

          this.setState({
            visible1: true,
            popupMsg: "Not an valid customer"
          });
        }
      }
    }
  }

  async storedata() {
    try {
      console.log("testid1", this.state.login_userid);

      const value = await AsyncStorage.getItem("login_id");
      console.log("loginId------", this.props.userDetails.userId);
      await AsyncStorage.setItem(
        "login_id_2",
        String(this.props.userDetails.userId)
      );
      this.props.navigation.navigate("index");
    } catch (error) {
      console.log(error);
      // Error saving data
    }
  }


    componentDidMount() {
    console.log(this.props);
    this.props.navigation.addListener("willFocus", () => {
      console.log("focus");
      //this.remember();
      console.log("emailAsyncStorage", this.state.email);
      console.log("emailAsyncStorage", this.state.password);
    });
  }



  signIn = async () => {    
    try {
      this.setState({ isSigninInProgress: true });
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      console.log("userInfo", userInfo);
      let googleDetails = new FormData();
      googleDetails.append('first_name', userInfo.user.givenName);
      googleDetails.append('last_name', userInfo.user.familyName);
      googleDetails.append('name', userInfo.user.name);
      googleDetails.append('email', userInfo.user.email);
      googleDetails.append('photo', userInfo.user.photo);
      googleDetails.append('google_signup_id', userInfo.user.id);
      //console.log('googleDetails', googleDetails);
      this.setState({spinner: true});
      this.props.googleLogin(googleDetails);
      
    } catch (error) {
        this.setState({ isSigninInProgress: false });
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
        console.log("SIGN_IN_CANCELLED-error", error);
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (f.e. sign in) is in progress already
        console.log("IN_PROGRESS-error", error);
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
        console.log("PLAY_SERVICES_NOT_AVAILABLE-error", error);
      } else {
        // some other error happened
        console.log("other-error", error);
      }
    }
  };

  render() {
    console.log("userdetails login render", this.props);

    return (
      <SafeAreaView>
        <StatusBar barStyle="light-content" backgroundColor="#1a1a1d" />
        <ImageBackground
          source={require("../../assets/images/mainbg.jpg")}
          style={styles.bodymain}
        >
          <KeyboardAvoidingView enabled behavior="padding">
            <ScrollView>
              <View style={[styles.container, { paddingTop: 16 }]}>
                <Spinner
                  visible={this.state.spinner}
                  textContent={"Loading..."}
                  textStyle={styles.spinnerTextStyle}
                />
                <TouchableOpacity style={styles.logo}>
                  <Image
                    source={require("../../assets/images/logo.png")}
                    resizeMode="contain"
                    style={{ width: 280, height: 80 }}
                  />
                </TouchableOpacity>

                <View style={[styles.mainform, { marginTop: 26 }]}>
                  <Text style={styles.heading}>
                    Login for quick reservations
                  </Text>

                  <TextInput
                    type="email"
                    style={styles.forminput}
                    placeholder="Email"
                    placeholderTextColor="#9590a9"
                    keyboardtype="email"
                    value={this.state.email}
                    onChangeText={email => this.emailHandler(email)}
                  />
                  <Text style={styles.error}>{this.state.email_error}</Text>

                  <TextInput
                    style={styles.forminput}
                    placeholder="Password"
                    placeholderTextColor="#9590a9"
                    value={this.state.password}
                    onChangeText={password => this.passwordHandler(password)}
                    secureTextEntry={true}
                  />
                  <Text style={styles.error}>{this.state.password_error}</Text>

                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      alignItems: "center",
                      paddingLeft: 10,
                      paddingBottom: 31
                    }}
                  >
                    <TouchableOpacity
                      onPress={() =>
                        props.navigation.navigate("ForgotPassword")
                      }
                    >
                      <Text
                        style={{
                          color: "#e25cff",
                          fontSize: 18
                        }}
                      >
                        Forgot password ?{" "}
                      </Text>
                    </TouchableOpacity>
                    <View style={{ flexDirection: "row" }}>
                      <CheckBox
                        style={{ color: "black" }}
                        checked={this.state.checkbox}
                        onPress={this.checkboxHandler}
                        checkedColor="#e25cff"
                      />
                      <Text
                        style={{
                          paddingTop: 15,
                          color: "#e25cff",
                          fontSize: 18,
                          right: 20
                        }}
                      >
                        Remember me
                      </Text>
                    </View>
                  </View>

                  {/* <TouchableOpacity
                    style={styles.forgetpass}
                    onPress={() =>
                      this.props.navigation.navigate("ForgotPassword")
                    }
                  >
                    <Text style={styles.pinktext}>Forgot Password</Text>
                  </TouchableOpacity> */}
                  <TouchableOpacity
                    style={styles.pinkbtn}
                    onPress={() => this.handleSubmit()}
                  >
                    <Text style={styles.btntext}>SIGN IN</Text>
                  </TouchableOpacity>

                  <Image
                    source={require("../../assets/images/or.png")}
                    resizeMode="contain"
                    style={styles.orline}
                  />

                  <View style={styles.container}>
                    <Dialog
                      visible={this.state.visible1}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      onTouchOutside={() => {
                        this.setState({ visible1: false });
                      }}
                      dialogStyle={{ width: "80%" }}
                      footer={
                        <DialogFooter>
                          <DialogButton
                            textStyle={{
                              fontSize: 14,
                              color: "#333",
                              fontWeight: "700"
                            }}
                            text="OK"
                            onPress={() => {
                              this.setState({ visible1: false });
                            }}
                          />
                        </DialogFooter>
                      }
                    >
                      <DialogContent>
                        <Text style={styles.popupText}>
                          {this.state.popupMsg}
                        </Text>
                      </DialogContent>
                    </Dialog>
                  </View>

                  <View style={styles.container}>
                    <Dialog
                      visible={this.state.visible2}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{ width: "80%" }}
                      footer={
                        <DialogFooter>
                          <DialogButton
                            textStyle={{
                              fontSize: 14,
                              color: "#333",
                              fontWeight: "700"
                            }}
                            text="OK"
                            onPress={this.fbLogin2}
                          />
                        </DialogFooter>
                      }
                    >
                      <DialogContent>
                        <Text
                          style={{
                            paddingTop: 5,
                            fontSize: 16,
                            fontWeight: "bold"
                          }}
                        >
                          Enter email Id
                        </Text>
                        <TextInput
                          type="email"
                          //style={styles.forminput}
                          placeholder="Email"
                          placeholderTextColor="#9590a9"
                          keyboardtype="email"
                          value={this.state.email}
                          onChangeText={email => this.emailHandler(email)}
                        />
                        <Text
                          style={{
                            paddingTop: 5,
                            fontSize: 12,
                            fontWeight: "bold",
                            color: "red"
                          }}
                        >
                          {this.state.email_error}
                        </Text>
                      </DialogContent>
                    </Dialog>
                  </View>

                  <View style={[styles.jsbtn, { marginTop: -20, }]}>
                    {/* <TouchableOpacity style={styles.googlebtn}>
                      <Text style={styles.btntext}>
                        <Icon name="google" style={{ marginRight: 10 }} /> With
                        GOOGLE
                      </Text>
                    </TouchableOpacity> */}
                  <GoogleSigninButton
                    style={{ width: 150, height: 52.5 }}
                    size={GoogleSigninButton.Size.Wide}
                    color={GoogleSigninButton.Color.Dark}
                    onPress={this.signIn}
                    disabled={this.state.isSigninInProgress} />
                    {this.state.facebookButton == 1 ? (
                      <TouchableOpacity>
                        <FBLogin
                          style={{
                            width: 165,
                            borderRadius: 6,
                            fontSize: 10,
                            height: 46
                          }}
                          ref={fbLogin => {
                            this.fbLogin = fbLogin;
                          }}
                          loginBehavior={FBLoginManager.LoginBehaviors.Native}
                          permissions={["email"]}
                          onLogin={this.fbLogin}
                          onLoginFound={function(e) {
                            console.log("error1", e);
                          }}
                          onLoginNotFound={function(e) {
                            console.log("error2", e);
                          }}
                          onLogout={function(e) {
                            console.log("logout", e);
                          }}
                          onPermissionsMissing={function(e) {
                            console.log("error3", e);
                          }}
                        />
                        {/* <Text style={styles.btntext}>
                        {" "}
                        <Icon
                          name="facebook"
                          style={{ marginRight: 10 }}
                        />{" "}
                        With FACEBOOK
                      </Text> */}
                      </TouchableOpacity>
                    ) : (
                      <TouchableOpacity style={styles.disableFbButton}>
                        <Text>Login with Facebook</Text>
                      </TouchableOpacity>
                    )}
                  </View>

                  {/* <Dialog
                    visible={this.state.visible3}
                    // onTouchOutside={() => {
                    //   this.closePopupbox();
                    // }}
                    dialogStyle={{ width: "90%", marginBottom: -160 }}
                    footer={
                      <DialogFooter>
                        <DialogButton
                          textStyle={{
                            fontSize: 14,
                            color: "#333",
                            fontWeight: "700"
                          }}
                          text="CANCEL"
                          onPress={this.close}
                        />
                      </DialogFooter>
                    }
                  >
                    <DialogContent>
                      <TouchableOpacity onPress={this.emailChoose}>
                        <Text style={styles.popupText}>
                          Email: {this.state.popUPemail}
                        </Text>
                        <Text style={styles.popupText}>Password: ******</Text>
                      </TouchableOpacity>
                    </DialogContent>
                  </Dialog> */}

                  <View style={[styles.bttext, { marginBottom: 40 }]}>
                    <Text style={styles.text}>New to Massaged Today? </Text>
                    <TouchableOpacity
                      onPress={() => this.props.navigation.navigate("Signup")}
                    >
                      <Text style={styles.pinktext}>Register Now</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.auth
});

export default connect(mapStateToProps, { login, fbLogin, googleLogin })(LoginScreen);
