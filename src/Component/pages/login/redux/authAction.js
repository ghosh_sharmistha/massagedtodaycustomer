import {LOGIN, SIGNUP, FBLOGIN, GOOGLElOGIN} from '../../../../redux/actions/types';
import Api from '../../../../Api/Api';
import AsyncStorage from '@react-native-community/async-storage';

export const login = userDetails => async dispatch => {
  try {
    const response = await Api.postApi('users/applogin', userDetails);
    if (response) {
      await AsyncStorage.setItem('login_id', String(response.userId));
      dispatch({type: LOGIN, payload: {...response, isLoggedIn: true}});
    }
  } catch (error) {
    console.log(error);
    dispatch({type: LOGIN, payload: {response: error, isLoggedIn: false}});
  }
};



export const signUp = userDetails => async dispatch => {
  try {
    const response = await Api.postApi('users/appsignup_name', userDetails);
    if (response) {
      dispatch({type: SIGNUP, payload: {...response}});
    }
  } catch (error) {
    dispatch({type: SIGNUP, payload: {response: error}});
  }
};

export const fbLogin = userDetails => async dispatch => {
  try {
    const response = await Api.postApi('users/facebook_login', userDetails);
    if (response) {
      console.log("action response", response );
      
      dispatch({type: FBLOGIN, payload: {...response}});
    }
  } catch (error) {
    dispatch({type: FBLOGIN, payload: {response: error}});
  }
};

export const googleLogin = userDetails => async dispatch => {
  try {
    const response = await Api.postApi('users/google_login', userDetails);
    if (response) {
      console.log("action response", response );
      
      dispatch({type: GOOGLElOGIN, payload: {...response}});
    }
  } catch (error) {
    dispatch({type: GOOGLElOGIN, payload: {response: error}});
  }
};



